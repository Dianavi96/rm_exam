<?php include("Controller/session.php");
$session = $_SESSION['login_admin'];


 ?>
<html style="height: auto;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Portal Empresas</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="manifest" href="manifest.json">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="libs/bower_components/bootstrap/dist/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="libs/bower_components/font-awesome/css/font-awesome.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="libs/bower_components/Ionicons/css/ionicons.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="libs/dist/css/AdminLTE.css">
    <link rel="stylesheet" href="libs/dist/css/skins/skin-blue.css">
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- jQuery 3 -->
    <script src="libs/bower_components/jquery/dist/jquery.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="libs/bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <!-- AdminLTE App -->
    <script src="libs/dist/js/adminlte.js"></script>
    <style>
        /* Text efect */
        .typewriter h1 {
            color: #b00016;
            overflow: hidden;
            /*border-right: .10em solid #00a65a; !* The typwriter cursor *!*/
            white-space: nowrap;
            margin: 0 auto;
            letter-spacing: .10em;
            animation: typing 3.1416s steps(30, end),
            blink-caret .5s step-end infinite;
        }

        @keyframes typing {
            from {
                width: 0
            }
            to {
                width: 100%
            }
        }

        @keyframes blink-caret {
            from, to {
                border-color: transparent
            }
            50% {
                border-color: #303f9f
            }
        }
    </style>
</head>
<body class="skin-blue sidebar-mini" style="background-color: #222d32">
<?php include("Controller/menu.php"); ?>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h2 class="box-title">Bienvenido <?= $_SESSION['login_name'] ?></h2>

                    </div>
                    <div class="box-body">
                        <div class="typewriter text-left">
                            <h1 style="font-size: 16px" class="text-uppercase text-bold text-red">
                                <i class="fa fa-lg fa-paper-plane-o" aria-hidden="true"></i>
                                SELECCIONE UN MÓDULO PARA INICIAR
                            </h1>
                        </div>
                    </div>
                </div>

                <div id="alertNav" class="alert alert-dismissible" style="background-color: #56f7a7">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>
                        <i class="fa fa-chrome" aria-hidden="true"></i> &nbsp; Se debe utilizar el navegador
                        <a href="https://www.google.com/intl/es-419_ALL/chrome/" class="text-blue">Google Chrome</a>
                    </h4>
                </div>
            </div>
        </div>
        <div class="row">
<? if( ($_SESSION['login_admin'] !== '1')){ ?>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header widget-user-header with-border text-center bg-blue">
                            <h3 class="box-title" style="color: white">
                                <i class="fa fa-balance-scale" aria-hidden="true"></i>
                            </h3>
                        </div>
                        <div class="box-body">
                            <button class="btn-block btn btn-success btn-lg text-bold uppercase"
                                    onclick="location.href='Administrator/userCreate.php'" style="margin-bottom: 6px">
                               Administración de usuarios
                            </button>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header widget-user-header with-border text-center bg-blue">
                            <h3 class="box-title" style="color: white">
                                <i class="fa fa-shield" aria-hidden="true"></i>
                            </h3>
                        </div>
                        <div class="box-body">
                            <button class="btn-block btn btn-success btn-lg text-bold uppercase "
                                    onclick="location.href='Administrator/userList.php'" style="margin-bottom: 6px">
                               Consulta de Usuarios
                            </button>

                        </div>
                    </div>
                </div>

            <? }else {?>
                    <div class="col-md-6">
        <div class="box box-solid">
            <div class="box-header widget-user-header with-border text-center bg-blue">
                <h3 class="box-title" style="color: white">
                    <i class="fa fa-shield" aria-hidden="true"></i>
                </h3>
            </div>
            <div class="box-body">
                <button class="btn-block btn btn-success btn-lg text-bold uppercase "
                        onclick="location.href='Administrator/userList.php'" style="margin-bottom: 6px">
                    Consulta de Usuarios
                </button>

            </div>
        </div>
    </div>
            <? }?>

        </div>

    </section>
</div>
<footer class="main-footer ">

    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0 <i class="fa fa-thumbs-o-up text-blue"></i>
    </div>
    <strong>Copyright © 2021 <a >Portal Empresa</a>
    </strong> All
    rights
    reserved.
</footer>
</body>
</html>

<script>
    let getBrowserInfo = function () {
        let ua = navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE ' + (tem[1] || '');
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
            if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    }

    $('#version').text(getBrowserInfo())
    if (getBrowserInfo().includes('Chrome')) {
        $('#alertNav').hide()
    }
</script>