<header class="main-header">
    <a href="/index.php" class="logo">
        <span class="logo-mini">RM</span>
        <span class="logo-lg"><b>Portal Empresas</b></span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" id="switch" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        <span class="hidden-xs"><?= $_SESSION['login_name'] ?></span>
                    </a>
                    <ul class="dropdown-menu" style="width: 330px">
                        <li class="user-header" style="height:auto">
                            <span class="text-uppercase" style="color: whitesmoke">
                                <?= $_SESSION['login_name'] . " " . $_SESSION['login_lastname'] ?>
                                <br>
                                <br>
                                <span class="label bg-navy" style="font-size: 12px">
                                <?= $_SESSION['login_level'] ?>
                                </span>
                                <span class="label label-warning" style="font-size: 12px">
                                <?= ($_SESSION['login_facility']) ?>
                                </span>
                            </span>
                        </li>
                        <li class="user-footer">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="../Administrator/userEdit.php" type="button"
                                       class="btn btn-flat btn-block">
                                        <i class="fa fa-edit fa-lg"></i> &nbsp
                                        Editar Perfil
                                    </a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="/Controller/login.php?logout" type="button"
                                       class="btn btn-warning btn-block">
                                        <i class="fa fa-sign-out fa-lg"></i> &nbsp
                                        Cerrar sesión
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar" style="height: auto;">
    <section class="sidebar">

        <!-- Menu -->
        <ul class="sidebar-menu">
            <li class="header text-center">NAVEGACIÓN PRINCIPAL</li>

            <li>
                <a href="../index.php">
                    <i class="fa fa-home"></i><span>Inicio</span>
                </a>

            </li>

            <li>
                <a href="http://10.72.5.47/">
                    <i class="fa fa-rocket" aria-hidden="true"></i><span></span>
                </a>

            </li>
        </ul>
    </section>
</aside>

