<?php

/****
 * Nombre del archivo:  DB_mysqli.php
 * Descripción:
 * Clase que implementa accesos a Base de Datos.
 * -MySQLi estilo orientado a objetos-
 ****/
date_default_timezone_set('America/Mexico_City');

class DBL
{
    protected static $connection;

    public function connect()
    {
        if (!isset(self::$connection)) {
            self::$connection = new mysqli('localhost', 'root', '', 'RM');
            if (self::$connection->connect_error) {
                die('Error de Conexión (' . self::$connection->connect_errno . ') '
                    . self::$connection->connect_error);
            }
            if (!self::$connection->set_charset("utf8")) {
                printf("Error cargando el conjunto de caracteres utf8: %s\n", self::$connection->error);
                exit();
            }
        }
        return self::$connection;
    }

    /*
     * Query the database
     *
     * @param $query The query string
     * @return mixed The result of query() as Jsonbtn-graph
     */
    public function queryStatement($query)
    {
        $rawData = array();
        $con = $this->connect();
        $result = $con->query($query);

        //metodo fetch devuelve un arreglo de los resultados
        while ($row = $result->fetch_assoc()) {
            $rawData[] = $row;
        }

        $result->free();
        //$result->close(); /* liberar y cerrar stmt */

        if (empty($rawData) == FALSE) {
            //json_encode convierte un arreglo u objeto a formato json
            $json_string = json_encode($rawData);
        } else {
            $json_string = '';
        }

        return $json_string;
    }


    /*
     * UPDATE
     *
     * @param $query The query string
     * @return mixed The result of query() as Json
     */
    public function updateStatement($query)
    {
        $con = $this->connect();
        if ($con->query($query) === TRUE) {
            $response = TRUE;
            //echo "Record updated successfully";
        } else {
            $response = FALSE;
            //echo "Error updating record: " . $conn->error;
        }
        return $response;
    }

    /*
     * SELECT STATEMENT
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public function select($query)
    {
        $rows = array();
        $con = $this->connect();
        $result = $con->query($query);
        if ($result === false) {
            return false;
        }
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    /*
     * SELECT ONE COLUMN STATEMENT
     *
     * @param $query The query string , $column The name of the column interested on 
     * @return bool False on failure / array Database rows on success
     */

    public function selectOneColumn($query, $column)
    {
        $rows = array();
        $con = $this->connect();
        $result = $con->query($query);

        if ($result === false) {
            return false;
        }
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row[$column];
        }
        return $rows;
    }


    public function one($query)
    {
        $rows = array();
        $con = $this->connect();
        $result = $con->query($query);

        if ($result === false) {
            return false;
        }

        while ($row = $result->fetch_array()) {
            $val = $row;
        }
        return $val;
    }

    /*
     * MAKE A TRANSACTION 
     *
     * @param $arrQueries 
     * @return bool False on failure / True success
     */

    public function makeTransaction($arrQueries)
    {

        $all_query_ok = true;

        $con = $this->connect();
        $con->autocommit(FALSE);

        foreach ($arrQueries as &$st) {

            if ($st != '')
                $con->query($st) ? null : $all_query_ok = false;
        }

        $all_query_ok ? $con->commit() : $con->rollback();

        $con->autocommit(TRUE);

        return $all_query_ok;
    }


    public function close()
    {

        $this->connect()->close();

        //$this->$connection ->close();
    }


    /*
     * Fetch the last error from the database
     * 
     * @return string Database error message
     */

    public function error()
    {
        $connection = $this->connect();
        return $connection->error;
    }

    /*
     * Quote and escape value for use in a database query
     *
     * @param string $value The value to be quoted and escaped
     * @return string The quoted and escaped string
     */

    public function quote($value)
    {
        $connection = $this->connect();
        return "'" . $connection->real_escape_string($value) . "'";
    }


    public function getFullName($user)
    {
        $conn = $this->connect();
        $query = "SELECT name,lastname FROM users WHERE user = '" . $user . "'";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $stmt->bind_result($name, $lastname);
        $stmt->fetch();
        $stmt->close();
        return $result = $name . ' ' . $lastname;
    }


//function to add new users:
    public function setUser($user)
    {
        $area_id = '1';
        $user_type = '0';
        if ($user["level"] === 'Jefe Calidad' || $user["level"] === 'Supervisor Calidad') {
            $user_type = '1';
        }

        $conn = $this->connect();
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $query = "INSERT INTO users(user, password, name, lastname,  user_type, gender, facility, area ,user_type) VALUES(?,?,?,?,?,?,?,?,?,?)";
        $stmt = $conn->prepare($query);
        $stmt->bind_param('sssssssiii', $user["number"], $user["password"], $user["name"], $user["lastname"], $user["facility"], $user["level"], $user["gender"], $user["admin"], $area_id, $user_type);
        $stmt->execute();
        $stmt->close();
        return true;
    }

    public function getUser($userId)
    {
        $conn = $this->connect();
        $query = "SELECT user,name,lastname,user_type FROM users WHERE user = '$userId';";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $stmt->bind_result($user, $name, $lastname, $facility_id);
        $result = array();
        while ($stmt->fetch()) {
            $result[] = array($user, $name, $lastname, $facility_id);
        }
        $stmt->close();
        return $result;
    }

    public function deleteUser($userId)
    {
        $conn = $this->connect();
        $query = "DELETE FROM users WHERE user = '$userId';";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $stmt->close();
        return true;
    }

}
