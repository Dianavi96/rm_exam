<?php

$serverName = '';
/****
 * Nombre del archivo:  session.php
 * Descripción:
 * Permite la navegación en el sistema con las credenciales válidas
 ****/
require("DB_mysqli.php");
session_start();
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado
if (!isset($_SESSION['login_user'])) {
    header("location:/RM/Controller/login.php");
}
include("funciones.php");
