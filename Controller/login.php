<?php
$destroy = false;
$error = false;
require("../Controller/DB_mysqli.php");
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_GET['logout'])) {
    $destroy = true;
    session_destroy();
} else if (isset($_SESSION['login_user'])) {
    header("location:/RM/index.php");
    die();
} else if (isset($_POST['submit'])) {
    $_user = $_POST['user'];
    $_password = $_POST['password'];
    $conexion1 = new DBL();
    $conexion1->connect();

    $sql = "SELECT *  FROM users WHERE users.user='$_user' AND users.password='$_password'";

    $Json = $conexion1->queryStatement($sql);
    $conexion1->close();
    if ($Json != '') {
        $resp1 = json_decode($Json);
        $Admin1 = ['Permisos'];
        $admin2 = ['cambios'];

        foreach ($resp1 as $e) {



            $_SESSION['login_user'] = $e->user;
            $_SESSION['login_password'] = $e->password;
            $_SESSION['login_name'] = $e->name;
            $_SESSION['login_lastname'] = $e->lastname;
            $_SESSION['login_area'] = $e->area_id;
            $_SESSION['login_level'] = $e->level;

            $_SESSION['user_type'] = $e->user_type;



        }
        header("location:/RM/index.php");
    } else {
        header('Location:/RM/login.php?logError=false');
    }
    die();
} else if (isset($_GET['logError'])) {
    $error = true;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Reina Madre</title>
    <?php include("../libs/stylesAndScripts.php") ?>
</head>
<body class="login-page">
<div class="login-box">
    <div class="login-logo">
        Portal Empresas
        <br>
        <b>RM</b>
    </div>
    <?php if ($destroy) { ?>
        <div class='alert alert-success alert-dismissible'>
            <h4>Sesión cerrada correctamente</h4>
        </div>
    <?php } elseif ($error) { ?>
        <div class="callout callout-danger">
            <h4>Error al iniciar sesión</h4>
            <p>Combinación errónea de nombre de usuario y contraseña.</p>
            <p>Inténtelo nuevamente</p>
        </div>
    <?php } ?>
    <div class="login-box-body">
        <p class="login-box-msg">Introduce tu información</p>
        <form id="form" name="" method="post" action="login.php">
            <div class="form-group has-feedback" id="" name="">
                <input type="text" id="user" name="user" class="form-control" placeholder="Número de empleado">
                <span class="fa fa-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback" id="" name="">
                <input type="password" id="password" name="password" class="form-control" placeholder="Contraseña">
                <span class="fa fa-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <input type="submit" name="submit" value="Entrar" class="btn btn-primary btn-block btn-flat">
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>