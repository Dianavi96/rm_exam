<?php

$db = new DBL();


function paginator($conn, $allQuery, $limit, $page, $counter, $lang)
{
    $paginator = '';
    //-------------------------[ Table Paginator ]--------------------------------------
    $page_result = mysqli_query($conn, $allQuery);
    $total_records = mysqli_num_rows($page_result);
    $total_pages = ceil($total_records / $limit);
    $paginator .= '<div class="row">';
    $paginator .= '<div class="col-md-6 text-left" style="color: #9C9C9C;padding-top: 20px"> <span>' . $counter . ' de ' . $total_records . $lang['r_registers'] . '</span></div>';
    $paginator .= '<div class="col-md-6 text-right"><div class="dataTables_paginate paging_simple_numbers"><ul class="pagination">';
    if ($page == 1) {
        $paginator .= '<li class="paginate_button previous disabled" ><a href="#" >' . $lang['btn_previous'] . '</a></li>';
    } else {
        $paginator .= '<li class="paginate_button previous" id="btn-previous"><a href="#" >' . $lang['btn_previous'] . '</a></li>';
    }

    $finded = false;
    $draw = 0;
    for ($i = 1; $i <= $total_pages; $i++) {
        if ($page == $i && $finded == false) {
            $finded = true;
            if ($i > 3) {
                $i = $i - 2;
            }
        }
        if ($page <= 3 && !$finded) {
            if ($page == $i) {
                $paginator .= '<li class="paginate_button pagination_link active"  id="' . $i . '"><a href="#" >' . $i . '</a></li>';
            } else {
                $draw++;
                $paginator .= '<li class="paginate_button pagination_link"  id="' . $i . '"><a href="#" >' . $i . '</a></li>';
            }
        }
        if ($finded) {
            if ($draw > 3) {
                break;
            }
            if ($page == $i) {
                $paginator .= '<li class="paginate_button pagination_link active"  id="' . $i . '"><a href="#" >' . $i . '</a></li>';
            } else {
                $draw++;
                $paginator .= '<li class="paginate_button pagination_link"  id="' . $i . '"><a href="#" >' . $i . '</a></li>';
            }
        }
    }
    if ($page == $total_pages) {
        $paginator .= '<li class="paginate_button next disabled" ><a href="#">' . $lang['btn_next'] . '</a></li>';
    } else {
        $paginator .= '<li class="paginate_button next" id="btn-next"><a href="#" >' . $lang['btn_next'] . '</a></li>';
    }
    $paginator .= '</ul></div></div><br /><br />';

    return $paginator;
}


