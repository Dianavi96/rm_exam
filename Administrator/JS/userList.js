$(document).ready(function () {
    load_data();

    function load_data() {
        let name = $('#inp-text').val();
        let facility = $('#inp-facility').val();
        let level = $('#inp-level').val();
        let page = $('#inp-page').val();
        let limit = $('#inp-limit').val();

        swal({
            title: 'Cargando ...',
            timer: 400,
            onOpen: () => {
                swal.showLoading()
            }
        });
        $.ajax({
            url: "tables/userList.php",
            method: "POST",
            data: {
                name: name,
                facility: facility,
                level: level,
                page: page,
                limit: limit
            },
            success: function (data) {
                $('#pagination_data').html(data);
                $('.main-sidebar').height($(document).outerHeight() - 50)
            }
        });
    }

    $(document).on('click', '.pagination_link', function () {
        let newPage = $(this).attr("id");
        $('#inp-page').val(newPage);
        load_data();
    });
    $(document).on('click', '#btn-search', function (e) {
        $('#inp-page').val(1);
        load_data();
    });
    $(document).on('click', '#btn-next', function (e) {
        let total = $('#inp-total-pages').val();
        let newPage = $('#inp-page').val();
        newPage++;
        if (total === newPage) {
        } else {
            $('#inp-page').val(newPage);
        }
        load_data();
    });
    $(document).on('click', '#btn-previous', function (e) {
        let newPage = $('#inp-page').val();
        newPage--;
        if (newPage === 1) {
            $(this).addClass('disabled');
        } else {
            $(this).removeClass('disabled');
        }
        $('#inp-page').val(newPage);
        load_data();
    });
    $('#inp-limit').on('change', function () {
        $('#inp-page').val(1);
        load_data();
    });
    $(document).on('click', '.delete-user', function (e) {
        let user = $(this).data('user')
        let userName = $(this).data('userName')
        swal({
            title: `¿Estas seguro de eliminar a ${userName}?`,
            text: "No podras revertir este paso",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Borrar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "tables/userList.php",
                    method: "POST",
                    data: {
                        delete: user,
                    },
                    success: function (data) {
                        load_data();
                    }
                });
                swal(
                    'Borrado!',
                    'El usuario ha sido eliminado',
                    'success'
                )
            }
        })
    });
    $(document).on('click', '.edit-user', function (e) {
        var user = $(this).attr('user');
        // alert(user);
        $('body').append($('<form/>')
            .attr({'action': 'userEdit.php', 'method': 'post', 'id': 'replacer'})
            .append($('<input/>')
                .attr({'type': 'hidden', 'name': 'user', 'value': user}))
        ).find('#replacer').submit()
    });
});

