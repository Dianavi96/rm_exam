$(document).ready(function () {
    $('.main-sidebar').height($(document).outerHeight() - 50);

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgView').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#btn-save").click(function () {
        var passwordX = $('#inp-password').val();
        var passwordY = $('#inp-password2').val();

        var form = $('#form-user');

        if (passwordX === '') {
            swal({
                position: 'center',
                type: 'warning',
                title: 'No se ha ingresado nueva contraseña',
                showConfirmButton: false,
                timer: 2500
            })
        } else {
            if (passwordX === passwordY) {
                form.submit();
            } else {
                swal({
                    position: 'center',
                    type: 'warning',
                    title: 'Las contraseñas no coinciden',
                    showConfirmButton: false,
                    timer: 2500
                })
            }
        }
    });

    $("form#form-user").submit(function (e) {
        e.preventDefault();
        let isAdmin = this.dataset.isAdmin
        let formData = new FormData(this)
        $.ajax({
            url: "userEdit.php",
            method: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function () {
                swal({
                    position: 'center',
                    type: 'success',
                    title: 'Información actualizada',
                    showConfirmButton: true,
                    timer: 2500
                });
                if (isAdmin!=='1'){
                    window.location.href = "../Controller/login.php?logout";
                }
            },
            error: function () {
                swal({
                    position: 'center',
                    type: 'warning',
                    title: 'Error al guardar',
                    showConfirmButton: false,
                    timer: 2500
                })
            }
        });
    });

    $("#imgProfile").change(function () {
        readURL(this);
    });

});