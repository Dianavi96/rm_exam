$(document).ready(function () {
    $('.main-sidebar').height($(document).outerHeight() - 50);

    function quitaAcentos(str) {
        for (var i = 0; i < str.length; i++) {
            if (str.charAt(i) == "á") str = str.replace(/á/, "a");
            if (str.charAt(i) == "é") str = str.replace(/é/, "e");
            if (str.charAt(i) == "í") str = str.replace(/í/, "i");
            if (str.charAt(i) == "ó") str = str.replace(/ó/, "o");
            if (str.charAt(i) == "ú") str = str.replace(/ú/, "u");
        }

    }
    $(document).on('click', '#btn-save', function () {
        let name = $('#inp-name').val();
        let lastname = $('#inp-last-name').val();
        let number = $("#inp-username").val();
        let facility = $('#inp-facility').val();
        let gender = $('input[name=gender]:checked').val();
        let admin = $('input[name=admin]:checked').val();
        let password = $('#inp-password').val();
        let password2 = $('#inp-password2').val();
        if (name === '' || lastname === '' || number === ''  || facility === '' || gender === '' || admin === '' || password === '') {
            swal({
                type: 'error',
                title: 'Error',
                text: 'Campos vacíos',
            })
            return str;

        } else if (password !== password2) {
            swal({
                type: 'error',
                title: 'Error',
                text: 'Las contraseñas no coinciden',
            })
        }
        else {
            $.ajax({
                url: "../Controller/userCreate.php",
                method: "POST",
                data: {
                    user: {
                        name: name,
                        lastname: lastname,
                        number: number,
                        level: level,
                        facility: facility,
                        gender: gender,
                        admin: admin,
                        password: password,
                        birthday: birthday,
                        email: email,
                        home: home,
                        cell: cell,
                        admission: admission
                    }
                },
                success: function (result) {
                    console.log(result);
                    $('#formNew')[0].reset();

                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'USUARIO REGISTRADO',
                        showConfirmButton: true,
                    })

                }
            });
        }
    });
});

