<?php
include("../Controller/session.php");



?>
<!DOCTYPE html>
<html style="height: auto;">
<head>
    <title>Lista de usuarios</title>
    <script src="../libs/sweetalert2/sweetalert2.js"></script>
    <?php include("../libs/stylesAndScripts.php") ?>
    <script src="JS/userList.js<?php echo $versionJS ?>"></script>
</head>
<body class="skin-blue sidebar-mini" style="height: auto; ">
<?php include("../Controller/menu.php"); ?>
<div class="content-wrapper">
    <section class="content-header">
        <button id="btn_back" class="btn btn-primary " onclick="location.href='panel.php'"><i
                    class="fa fa-fw fa-arrow-left"></i>&nbsp
            Regresar>
        </button>
        <button id="btn_back" class="btn btn-success" onclick="location.href='userCreate.php'"><i
                    class="fa fa-fw fa-user-plus"></i>&nbsp
            Crear Usuarios
        </button>
        <ol class="breadcrumb">
            <li>
                <a href="#" onclick="location.href='../index.php'"><i class="fa fa-dashboard"></i>Inicio</a>
            </li>
            <li class="active">Lista de usuarios
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-lg-8">
                                <h3 class="box-title">
                                    <i class="fa fa-list"></i>
                                    Lista de usuarios
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <br>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="box box-danger">
                                    <div class="box-header with-border">
                                        <i class="fa fa-fw fa-search"></i>
                                        <h1 class="box-title">Búsqueda</h1>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                                    data-toggle="tooltip" title="" data-original-title="Collapse">
                                                <i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <form method="post">
                                            <div class="form-group">
                                                <label for="inp-text">Nombre </label>
                                                <input type="text" class="form-control" id="inp-text">
                                            </div>
                                            <div class="form-group">
                                                <label for="inp-facility">Centro de trabajo</label>
                                                <select class="form-control" id="inp-facility">
                                                    <option value="" selected
                                                            disabled>Seleccione unaopción </option>
                                                    <option value="">Empresa A</option>
                                                    <option value="">Empresa B</option>
                                                    <option value="">Empresa C</option>
                                                    <option value="">Empresa D</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="inp-level">Departamento</label>
                                                <select class="form-control" id="inp-level">
                                                    <option value="" selected
                                                            disabled>Seleccione unaopción </option>
                                                    <option value="">Departamento A</option>
                                                    <option value="">Departamento B</option>
                                                    <option value="">Departamento C</option>
                                                    <option value="">Departamento D</option>
                                                </select>
                                            </div>
                                            <input type="hidden" id="inp-page" value="1">
                                            <input type="hidden" id="inp-user">

                                            <br>
                                            <div id="btn-search" class="btn btn-primary col-lg-12">
                                                <i class="fa fa-fw fa-search"></i>&nbsp;
                                               Buscar
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <!-- create table here-->
                                <div class="box box-danger" id="pagination_data">
                                    <div class="box-body no-padding">
                                        <div class="table-responsive" id="pagination_data">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10 text-right">
                                        <label>
                                            <i class="fa fa-fw fa-list"></i>Numero de registros
                                        </label>
                                    </div>
                                    <div class="col-xs-2 text-right">
                                        <select id="inp-limit" class="form-control">
                                            <option value="5">5</option>
                                            <option value="10" selected>10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

</body>
</html>