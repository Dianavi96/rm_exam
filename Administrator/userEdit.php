<?php
include("../Controller/session.php");



    if (isset($_POST['user'])) {
        $user = $db->getUser($_POST['user']);
        $isAdmin = true;
    } else {
        $user = $db->getUser($_SESSION['login_user']);
        $isAdmin = false;
    }
    $user = $user[0];
    ?>

    <!DOCTYPE html>
    <html>
    <head>
        <title>Editar usuario</title>
        <?php include("../libs/stylesAndScripts.php") ?>
        <script src="../libs/sweetalert2/sweetalert2.js"></script>
    </head>
    <body class="skin-blue sidebar-mini ">
    <?php include("../Controller/menu.php"); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Administrador</a></li>
                <li class="active">Editar usuario</li>
            </ol>
            <input id='post-facility' type="hidden" value='<?= $user[3] ?>'>
            <input id='post-img' type="hidden" value='<?= $user[4] ?>'>
            <input id='post-area' type="hidden" value='<?= $user[5] ?>'>
            <input id='post-level' type="hidden" value='<?= $user[6] ?>'>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box-success" style="margin-left: 10%;margin-right: 10%">
                <div class="register-logo">
                    <b><i class="fa fa-plus"></i> Editar</b>usuario
                </div>
                <div class="register-box-body">
                    <div class="row">
                        <form id="form-user" data-is-admin="<?= $isAdmin ?>">
                            <div class="<?= ($isAdmin) ? 'col-md-6' : 'col-xs-12'; ?>">
                                <div class="form-group has-feedback">
                                    <label>Nombre</label>
                                    <input id="inp-name" name="inp-name" class="form-control" placeholder="Nombre"
                                           type="text"
                                           value="<?= $user[1]; ?>">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <label>Apellidos</label>
                                    <input id="inp-last-name" name="inp-last-name" class="form-control"
                                           placeholder="Apellidos" type="text"
                                           value="<?= $user[2]; ?>">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <label>Numero de colaborador</label>
                                    <input id="inp-collaborator"
                                           name="inp-collaborator"
                                           class="form-control"
                                           placeholder="Generado automaticamente"
                                           type="text" readonly value="<?= $user[0]; ?>">
                                    <span class="glyphicon glyphicon glyphicon-ok form-control-feedback"></span>
                                </div>
                                <?php if ($isAdmin){ ?>
                                <div class="form-group has-feedback">
                                    <label>Departamento</label>
                                    <select class="form-control" id="inp-area">
                                        <option value="" selected disabled>Seleccione el centro de trabajo al que pertence</option>
                                        <option value="">Departamento A</option>
                                        <option value="">Departament B</option>
                                        <option value="">Departament C</option>
                                        <option value="">Departament D</option>
                                    </select>
                                </div>
                                <div class="form-group has-feedback ">
                                    <label>Centro de trabajo</label>
                                    <select class="form-control" id="inp-facility">
                                        <option value="" selected disabled>Seleccione el centro de trabajo al que pertence</option>
                                        <option value="">Empresa A</option>
                                        <option value="">Empresa B</option>
                                        <option value="">Empresa C</option>
                                        <option value="">Empresa D</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    <label>¿Otorgar permisos de Administrador?</label>
                                    <div class="row text-left">
                                        <div class="col-md-6 "
                                             style="height: 24px; margin-top: 5px; margin-bottom: 5px">
                                            <input class="iradio_flat-green" type="radio" name="admin" value="0"
                                                   checked>
                                            NO
                                        </div>
                                        <div class="col-md-6" style="height: 24px; margin-top: 5px; margin-bottom: 5px">
                                            <input class="iradio_flat-green" type="radio" name="admin" value="1">
                                            SI
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="form-group has-feedback">
                                    <label>Nueva contraseña</label>
                                    <input id="inp-password"
                                           name="inp-password"
                                           class="form-control"
                                           placeholder="Contraseña"
                                           type="password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <label>Repetir contraseña</label>
                                    <input id="inp-password2"
                                           name="inp-password2"
                                           class="form-control"
                                           placeholder="Repetir Contraseña"
                                           type="password">
                                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                </div>

                                <button id="btn-save" type="button" class="btn btn-primary btn-block">
                                    <b>ACTUALIZAR USUARIO</b>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
        </section>
    </div>

    </body>
    </html>

