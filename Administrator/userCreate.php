<?php include("../Controller/session.php");

?>
<!DOCTYPE html>
<html>
<head>
    <title>Alta de usuario</title>
    <?php include("../libs/stylesAndScripts.php") ?>
    <script src="../libs/sweetalert2/sweetalert2.js"></script>
    <script src="JS/userCreate.js<?php echo $versionJS ?>"></script>
</head>
<body class="skin-blue sidebar-mini ">
<?php include("../Controller/menu.php"); ?>
<div class="content-wrapper">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Administrador</a></li>
            <li class="active">Dar de alta nuevo usuario</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box-success" style="margin-left: 10%;margin-right: 10%">
            <div class="register-logo">
                <b><i class="fa fa-plus"></i> Alta</b>usuario
            </div>
            <div class="register-box-body">
                <p class="login-box-msg">Registrar un nuevo usuario para Portal empresas</p>
                <form id="formNew" class="row" method="post">
                    <div class="col-md-6">
                        <div class="form-group has-feedback">
                            <label>Nombre</label>
                            <input id="inp-name" class="form-control" placeholder="Nombre" type="text">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Apellidos</label>
                            <input id="inp-last-name" class="form-control" placeholder="Apellidos" type="text">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Número de empleado</label>
                            <input id="inp-username" class="form-control" placeholder="Número de empleado"
                                   type="number">
                            <span class="glyphicon glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Fecha de nacimiento</label>
                            <input id="inp-birthday" class="form-control" placeholder="Fecha de nacimiento (dd-MM-AAA)"
                                   type="text">
                            <span class="glyphicon glyphicon glyphicon-calendar form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Correo Electronico </label>
                            <input id="inp-email" class="form-control" placeholder="Correo Electronico"
                                   type="text">
                            <span class="glyphicon glyphicon glyphicon-heart-empty form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Centro de trabajo</label>
                            <select class="form-control" id="inp-facility">
                                <option value="" selected disabled>Seleccione el centro de trabajo al que pertence</option>
                                <option value="">Empresa A</option>
                                <option value="">Empresa B</option>
                                <option value="">Empresa C</option>
                                <option value="">Empresa D</option>
                            </select>
                        </div>
                        <div class="form-group has-feedback ">
                            <label>Fecha de ingreso</label>
                            <input id="inp-admission" class="form-control" placeholder="admissiom"
                                   type="text">
                            <span class="glyphicon glyphicon glyphicon-calendar form-control-feedback"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group has-feedback">
                            <label>Departamento al que pertenece</label>
                            <select class="form-control" id="inp-area">
                                <option value="" selected disabled>Seleccione el centro de trabajo al que pertence</option>
                                <option value="">Departamento A</option>
                                <option value="">Departament B</option>
                                <option value="">Departament C</option>
                                <option value="">Departament D</option>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Telefono Celular</label>
                            <input id="inp-cellphone" class="form-control" placeholder="Telefono Celular"
                                   type="text">
                            <span class="glyphicon glyphicon glyphicon-phone form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Telefono de casa</label>
                            <input id="inp-phoneHome" class="form-control" placeholder="Telefono de casa"
                                   type="text">
                            <span class="glyphicon glyphicon glyphicon-phone-alt form-control-feedback"></span>
                        </div>
                        <div class="form-group ">
                            <label>Género</label>
                            <div>
                                <div class="row text-left">
                                    <div class="col-md-6 " style="height: 24px; margin-top: 5px; margin-bottom: 5px">
                                        <input class="" type="radio" name="gender" value="M"
                                               checked>
                                        MASCULINO
                                    </div>
                                    <div class="col-md-6" style="height: 24px; margin-top: 5px; margin-bottom: 5px">
                                        <input class="" type="radio" name="gender" value="F">
                                        FEMENINO
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label>¿Otorgar permisos de Administrador?</label>
                            <div class="row text-left">
                                <div class="col-md-6 " style="height: 24px; margin-top: 5px; margin-bottom: 5px">
                                    <input class="" type="radio" name="admin" value="0"
                                           checked>
                                    NO
                                </div>
                                <div class="col-md-6" style="height: 24px; margin-top: 5px; margin-bottom: 5px">
                                    <input class="" type="radio" name="admin" value="1">
                                    SI
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Contraseña</label>
                            <input id="inp-password" class="form-control" placeholder="Contraseña" type="password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Repetir contraseña</label>
                            <input id="inp-password2" class="form-control" placeholder="Repetir Contraseña"
                                   type="password">
                            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                        </div>
                        <button id="btn-save" type="button" class="btn btn-primary btn-block" style="margin-top: 36px">
                            <b>REGISTRAR USUARIO</b>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<?php include("../Controller/footer.php"); ?>
</body>
</html>

