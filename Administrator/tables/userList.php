<?php
include("../../Controller/session.php");


if (isset($_POST['delete'])) {
    $result = $db->deleteUser($_POST['delete']);
    echo $result;
} else {

    $name = (isset($_POST['name'])) ? $_POST['name'] : '';
    $facility = (isset($_POST['facility'])) ? $_POST['facility'] : '';
    $area = (isset($_POST['area'])) ? $_POST['area'] : '';
    $page = (isset($_POST['page'])) ? $_POST['page'] : 1;
    $limit = (isset($_POST['limit'])) ? $_POST['limit'] : '';


//-------------------------[ Query ]----------------------------------------------
    $start_from = ($page - 1) * $limit;
    $conn = $db->connect();
    $query = "SELECT user,concat(name,' ',lastname) as fullname,  facility, area FROM users ";
    $checkWhere = true;
    if ($name !== '') {
        $query .= ($checkWhere) ? "WHERE " : "AND ";
        $checkWhere = false;
        $query .= "concat(name,' ',lastname) LIKE '%$name%' ";
    }
    if ($facility !== '') {
        $query .= ($checkWhere) ? "WHERE " : "AND ";
        $checkWhere = false;
        $query .= "facility_id='$facility' ";
    }
    if ($area !== '') {
        $query .= ($checkWhere) ? "WHERE " : "AND ";
        $checkWhere = false;
        $query .= "area='$area' ";
    }

    $allQuery = $query;
    $query .= "ORDER BY facility_id, level LIMIT $start_from, $limit ;";
//print_r($query);
//-------------------------[ Fill Table ]------------------------------------------
    $result = mysqli_query($conn, $query);
    $output = '';
    ob_start();
    ?>
    <div class='box-body no-padding'>
        <table class='table table-striped table-responsive'>
            <thead>
            <tr>
                <th>No.</th>
                <th>Permisos de administrador</th>
                <th>Nombre completo</th>
                <th>Centro de trabajo</th>
                <th>Departamento</th>
                <th> Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $counter = 0;
            $check = true;
            while ($row = mysqli_fetch_array($result)) {
            $check = false;
            $counter++; ?>
            <tr>
                <td><?= $row["user"] ?></td>
                <td><?= (($row["user_type"] === '1') ? '&nbsp;<span class="label label-warning">Admin</span>' : ' '); ?></td>
                <td><?= $row["fullname"] ?></td>
                <td><?= $row["facility_id"] ?></td>
                <td><?= $row["area"] ?></td>
                    <a user="<?= $row["user"] ?>"
                       class="edit-user"
                       data-toggle="tooltip"
                       title="Editar">
                        <i class="fa fa-lg fa-fw fa-pencil"></i>
                    </a>
                    <a class="delete-user" data-toggle="tooltip" title="Eliminar"
                       data-user-name="<?= $row["fullname"] ?>"
                       data-user="<?= $row["user"] ?>">
                        <i class="fa fa-lg fa-fw fa-remove text-red"></i>
                    </a>

                <?php } ?>
            </tr>
            </tbody>
        </table>
    </div>
    <?php
    if ($check) { ?>
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> Atención!</h4>
            No se encontraron registros con estos parametros.
        </div>
    <?php } else {
        $output = ' ' . ob_get_clean();
//-------------------------[ Table footer ]--------------------------------------
        $page_result = mysqli_query($conn, $allQuery);
        $total_records = mysqli_num_rows($page_result);
        $total_pages = ceil($total_records / $limit);
        $output .= '<div class="box-footer"><div class="row">';
        $output .= '<div class="col-md-6 text-left" style="color: #9C9C9C;padding-top: 20px"> <span>' . $counter . ' de ' . $total_records . 'Registros'. '</span></div>';
        $output .= '<div class="col-md-6 text-right"><div class="dataTables_paginate paging_simple_numbers"><ul class="pagination">';
        if ($page == 1) {
            $output .= '<li class="paginate_button previous disabled" ><a href="#" >' . 'Anterior' . '</a></li>';
        } else {
            $output .= '<li class="paginate_button previous" id="btn-previous"><a href="#" >' . 'Anterior'. '</a></li>';
        }

        $finded = false;
        $draw = 0;
        for ($i = 1; $i <= $total_pages; $i++) {
            if ($page == $i && $finded == false) {
                $finded = true;
                if ($i > 3) {
                    $i = $i - 2;
                }
            }
            if ($page <= 3 && !$finded) {
                if ($page == $i) {
                    $output .= '<li class="paginate_button pagination_link active"  id="' . $i . '"><a href="#" >' . $i . '</a></li>';
                } else {
                    $draw++;
                    $output .= '<li class="paginate_button pagination_link"  id="' . $i . '"><a href="#" >' . $i . '</a></li>';
                }
            }
            if ($finded) {
                if ($draw > 3) {
                    break;
                }
                if ($page == $i) {
                    $output .= '<li class="paginate_button pagination_link active"  id="' . $i . '"><a href="#" >' . $i . '</a></li>';
                } else {
                    $draw++;
                    $output .= '<li class="paginate_button pagination_link"  id="' . $i . '"><a href="#" >' . $i . '</a></li>';
                }
            }
        }

        if ($page == $total_pages) {
            $output .= '<li class="paginate_button next disabled" ><a href="#">' . 'Siguiente' . '</a></li>';
        } else {
            $output .= '<li class="paginate_button next" id="btn-next"><a href="#" >' . 'Siguiente'. '</a></li>';
        }


        $output .= '</ul></div></div><br /><br />';
        $output .= '</div></div></div></div>';

        echo $output;
    }
}